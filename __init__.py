from trytond.pool import Pool
from .production import Production


def register():
    Pool.register(
        Production,
        module='production_stock', type_='model')