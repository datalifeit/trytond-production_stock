# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['Production']

__metaclass__ = PoolMeta


class Production:
    __name__ = 'production'

    stock = fields.Function(fields.One2Many('stock.lot.location-quantity', None, 'Stock',
                                            depends=['outputs'], readonly=True),
                            'get_stock_by_location')

    is_stored_at = fields.Function(
        fields.Boolean('Is stored at',
                       readonly=False, select=True, help='Is stored at'),
        'get_is_stored_at', searcher='search_is_stored_at')

    @classmethod
    def __setup__(cls):
        super(Production, cls).__setup__()
        cls._error_messages.update(
            {'lot_required': 'Output Product "%(product)s" lot must be required.'})

    @classmethod
    def write(cls, *args):
        super(Production, cls).write(*args)
        for production in sum(args[::2], []):
            if production.state != 'draft':
                production.create_output_lots()

    def create_output_lots(self):
        created_lots = []
        for output in self.outputs:
            if not output.product.lot_is_required(output.from_location,
                                                  output.to_location):
                continue
            if output.lot:
                continue
            lot = self.get_output_lot(output)
            lot.save()
            output.lot = lot
            output.save()
            created_lots.append(lot)
        return created_lots

    def get_output_lot(self, output):
        pool = Pool()
        Lot = pool.get('stock.lot')

        number = self._get_output_lot_number()
        lot = Lot.search([('product', '=', output.product),
                          ('number', '=', number)])
        if lot:
            lot, = lot
        else:
            lot = Lot(product=output.product, number=number)

        if hasattr(Lot, 'expiry_date'):
            if output.product.expiry_time:
                input_expiry_dates = [i.lot.expiry_date for i in self.inputs
                                      if i.lot and i.lot.expiry_date]
                if input_expiry_dates:
                    lot.expiry_date = min(input_expiry_dates)
                else:
                    expiry_date = lot.on_change_product().get('expiry_date')
                    if expiry_date:
                        lot.expiry_date = expiry_date
        return lot

    def _get_output_lot_number(self):
        return self.code

    def get_stock_by_location(self, name=None, stock_date_end=None):
        tran = Transaction()
        pool = Pool()
        Lot = pool.get('stock.lot')
        Product = pool.get('product.product')

        lot_ids = [o.lot.id for o in self.outputs if o.lot]
        if not lot_ids:
            return {}
        default_date = tran.context.get('stock_date_end')
        default_date = default_date if default_date else datetime.date.max
        context = {'stock_date_end': stock_date_end if stock_date_end else default_date}

        with tran.set_context(context):
            pbl = Lot.lots_by_location(lot_ids=lot_ids,
                                       with_childs=False)
        result = []
        for key, quantity in pbl.iteritems():
            if quantity == 0:
                continue
            ps = {'location': key[0],
                  'product': key[1],
                  'lot': key[2],
                  'quantity': quantity,
                  'uom': Product(key[1]).default_uom.id}
            result.append(ps)
        return result

    @staticmethod
    def _lots_by_location(lot_ids=None, location_ids=None):
        tran = Transaction()
        pool = Pool()
        lot_pool = pool.get('stock.lot')

        context = dict()
        default_date = tran.context.get('stock_date_end')
        if not default_date:
            context['stock_date_end'] = datetime.date.max

        if not location_ids:
            location_ids = tran.context.get('location_ids', None)

        with tran.set_context(context):
            pbl = lot_pool.lots_by_location(lot_ids=lot_ids,
                                            location_ids=location_ids,
                                            with_childs=False)

        return pbl

    def get_is_stored_at(self, name=None):
        pbl = Production._lots_by_location(lot_ids=[self.code])
        sum_quantity = 0
        for _, quantity in pbl.iteritems():
            sum_quantity += quantity

        return sum_quantity

    @classmethod
    def search_is_stored_at(cls, name, clause):
        pbl = cls._lots_by_location()
        _, operator, values = clause
        if operator not in ['=', '!=', 'in', 'not in']:
            raise ValueError('Search by Is stored at field with given operator is not supported.')
        if operator in ['=', '!='] and not isinstance(values, bool):
            raise ValueError('Search by Is stored at field with given value is not supported.')
        if operator in ['in', 'not in'] and not isinstance(values, list):
            raise ValueError('Search by Is stored at field with given value is not supported.')

        lot_numbers = list()
        lot_ids = list()
        for key, quantity in pbl.iteritems():
            lot_ids.append(key[2])
        if lot_ids:
            lot_pool = Pool().get('stock.lot')
            lots = lot_pool.browse(lot_ids)
            lot_numbers = [l.number for l in lots]

        if not isinstance(values, list):
            values = [values]

        if all(s in values for s in [True, False]):
            if operator == 'in':
                domain = None
            else:
                domain = [('id', '=', -1)]
        elif any(s in values for s in [True]):
            if operator in ('in', '='):
                domain = [('code', 'in', lot_numbers)]
            else:
                domain = [('code', 'not in', lot_numbers)]
        else:
            if operator in ('in', '='):
                domain = [('code', 'not in', lot_numbers)]
            else:
                domain = [('code', 'in', lot_numbers)]

        return domain